public class Calculator {
    static double result;
    public static double add(double number1,double number2){
        result = number1 + number2;
       return result;
    }
    public static double subtract(double number1,double number2){
        result = number1 - number2;
        return result;
    }
    public static double multiply(double number1,double number2){
        result = number1 * number2;
        return result;
    }
    public static double divide(double number1,double number2){
        result = number1 / number2;
        return result;
    }

}
